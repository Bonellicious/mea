<?php
/**
 * Template Name: Experience
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'exp-template'); ?>
<?php endwhile; ?>
