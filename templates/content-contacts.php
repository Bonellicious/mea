<section class="contacts" id="contacts">
  <div class="container">
    <div class="barra">
    </div>
  </div>
  <header class="clearfix">
    <div class="container Hcontenitore">
      <div class="row">
        <?php
          $con = get_field('descrizione_ctc');
          if ($con) {
            $cl = 'col-xs-12 col-md-6';
          } else {
            $cl = 'col-xs-12 col-md-12 cent';
          }
        ?>
        <div class="<?php echo $cl; ?> hstyle">
          <h2>
            <?php echo get_field('titolo_ctc'); ?>
          </h2>
        </div>
        <?php
        if (get_field('descrizione_ctc')) {
          echo '<div class="col-xs-12 col-md-6 pstyle HCon">';
            echo '<p class="Htxt">'.get_field('descrizione_ctc').'</p>';
          echo '</div>';
        }
        ?>
      </div>  
    </div>
  </header>
  <?php
    $sot = get_field('sottotitolo_ctc');
    if ($sot) {
      echo '<div class="container">';
      echo '<div class="row sottHeader">';
      echo $sot;
      echo '</div>';
      echo '</div>';
    }
  ?>
  <div class="cont">
        <?php echo get_field('testo_ctc'); 
        $btn = get_field('titolo_bottone_ctc');
        if ($btn) {
          echo '<a href="'.get_field('file_ctc').'" download><button>'.get_field('titolo_bottone_ctc').'</button></a>';
        }
        ?>
  </div>
</section>