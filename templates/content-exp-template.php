<div class="container">
    <div class="row">
        <?php if (get_field('attivare_sidebar')) {
            $clCNT = 'col-xs-12 col-md-7';
        } else {
            $clCNT = 'col-xs-12 col-md-8 col-md-offset-2';
        }?>
        <div class="contenuto <?php echo $clCNT; ?>">
            <?php the_content(); ?>
                <?php if (!get_field('attivare_sidebar')) {
                if (get_field('campo_credits')) {
                    echo '<div class="credits">';
                        echo get_field('campo_credits');
                    echo '</div>';
                }
            } ?>
        </div>

        <?php if (get_field('attivare_sidebar')) { ?>
            <aside class="sidebar col-xs-12 col-md-5">
                <div class="clearfix">
                <?php
                if (get_field('yt_v') == 'youtube') { 
                    if (get_field('video_url')) { ?>
                        <div class="video">
                            <iframe height="285" src="https://www.youtube.com/embed/<?php echo get_field('video_url') ?>" frameborder="0" allowfullscreen>
                            </iframe>
                        </div>
                    <?php } ?>
                <?php } else { ?>
                    <div class="video">
                    <iframe src="https://player.vimeo.com/video/<?php echo get_field('vimeo_url') ?>" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen>
                    </iframe>
                        </div>
                <?php }?>    
                
                <?php if (get_field('gallery')) { ?>
                    <div class="grid clearfix" id="lightgallery">
                        <?php
                        // print_r(get_field('gallery'));
                        foreach(get_field('gallery') as $galleria) {
                            echo '<a class="col-xs-4 grid__item img-wrap" href="'.$galleria[sizes][large].'">';
                                echo '<img src="'.$galleria[sizes][medium].'" alt="img01" />';
                            echo '</a>';

                        }
                        
                        ?>
                        
                    </div>
                <?php } ?>
                <?php if (get_field('campo_credits')) {
                    echo '<div class="credits">';
                        echo get_field('campo_credits');
                    echo '</div>';
                } ?>
                </div>
            </aside>
        <?php } ?>
    </div>
</div>
<?php get_template_part('templates/content', 'experience'); ?>