<section class="experience" id="experience">
  <div class="container">
    <div class="barra">
    </div>
  </div>
  <header class="clearfix">
    <div class="container Hcontenitore">
      <div class="row">
      <?php
          $con = get_field('descrizione_xp',2);
          if ($con) {
            $cl = 'col-xs-12 col-md-6';
          } else {
            $cl = 'col-xs-12 col-md-12 cent';
          }
        ?>
        <div class="<?php echo $cl; ?> hstyle">
          <h2>
            <?php echo get_field('titolo_xp',2); ?>
          </h2>
        </div>
        <?php
        if (get_field('descrizione_xp',2)) {
          echo '<div class="col-xs-12 col-md-6 pstyle HCon">';
            echo '<p class="Htxt">'.get_field('descrizione_xp',2 ).'</p>';
          echo '</div>';
        }
        ?>
      </div>  
    </div>
  </header>
  <?php
    $sot = get_field('sottotitolo_xp');
    if ($sot) {
      echo '<div class="container">';
      echo '<div class="row sottHeader">';
      echo $sot;
      echo '</div>';
      echo '</div>';
    }
  ?>
  <div class="cont">
  <?php
      $pagine = get_field('pagine_link',2);
      if ($pagine) {
        echo '<div class="container">';
        echo '<div class="row">';
        foreach($pagine as $pagina) {
          $url = $pagina['immagine']['sizes']['experience-thumbnail'];
          echo '<a href="'.get_the_permalink($pagina[pagina]).'" class="Hcontenitore continua">';
          echo '<div class="col-xs-6 col-md-4 singleLink HCon">';
          echo '<div class="colorBg move" style="background:#333;">';
          echo '</div>';
          echo '<div class="colorBg">';
          echo '</div>';
          echo '<div class="imgBg" style="background:url('.$url.') center center no-repeat; background-size:cover; opacity:0.5;">';
          echo '</div>';
          echo '<h3 class="Htxt">'.get_the_title($pagina[pagina]).'';
          if (get_field('sottotitolo',$pagina[pagina])) {
            echo '<span>'.get_field('sottotitolo',$pagina[pagina]).'<span></h3>';
          }
          echo '</div>';
          echo '</a>';
        }
        echo '</div>';
        echo '</div>';
      }
    ?>
  </div>
</section>