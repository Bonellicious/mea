<section class="activities" id="activities">
  <div class="container">
    <div class="barra">
    </div>
  </div>
  <header class="clearfix">
    <div class="container Hcontenitore">
      <div class="row">
      <?php
          $con = get_field('descrizione_act');
          if ($con) {
            $cl = 'col-xs-12 col-md-6';
          } else {
            $cl = 'col-xs-12 col-md-12 cent';
          }
        ?>
        <div class="<?php echo $cl; ?> hstyle">
          <h2>
            <?php echo get_field('titolo_act'); ?>
          </h2>
        </div>
        <?php
        if (get_field('descrizione_act')) {
          echo '<div class="col-xs-12 col-md-6 pstyle HCon">';
            echo '<p class="Htxt">'.get_field('descrizione_act').'</p>';
          echo '</div>';
        }
        ?>
      </div>  
    </div>
  </header>
  <?php
  $sot = get_field('sottotitolo_act');
  if ($sot) {
    echo '<div class="container">';
    echo '<div class="row sottHeader">';
    echo $sot;
    echo '</div>';
    echo '</div>';
  }
  ?>
  <div class="cont">
    <div class="container">
      <div class="row">
        <?php
          $blocchi = get_field('blocchi');
          if ($blocchi) { 
            foreach($blocchi as $blocco) {
              echo '<div class="col-xs-12 col-md-3 singleBlocco">';
              echo '<img src="'.$blocco[immagine][sizes][medium].'">';
              echo '<div class="posizionato">';
              echo '<h3>'.$blocco[titolo].'</h3>';
              echo '</div>';
              echo '<p>'.$blocco[testo].'</p>';
              if ($blocco[link]) {
                echo '<a class="hidden-lg hidden-md" href="'.$blocco[link_di_puntamento].'">'.$blocco[titolo_del_link].'</a>';
              }
              echo '</div>';
            }
          }
        ?>
        </div>
          
        <?php
        // BOTTONI
          $blocchi = get_field('blocchi');
          if ($blocchi) { 
            echo '<div class="bloccoBTN row">';
            foreach($blocchi as $blocco) {
              echo '<div class="col-xs-12 col-md-3 singleBloccoBTN">';
              if ($blocco[link]) {
                echo '<a class="hidden-xs hidden-sm" href="'.$blocco[link_di_puntamento].'">'.$blocco[titolo_del_link].'</a>';
              }
              echo '</div>';
            }
            echo '</div>';
          }
        ?>
      </div>
    </div>
</section>