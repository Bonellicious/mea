<header class="banner">
  <div class="container">
    <div class="row">
      <!-- menu per il mobile  -->
      <?php
        if (has_nav_menu('primary_navigation')) :
          echo '<div class="mob-menu hidden-lg hidden-md">';
          wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav navMob']);
          $social = get_field('social_network','options');
          if ($social) {
            echo '<ul class="socialNetworkMob">';
            foreach($social as $socialnetwork) {
              $output = '
                <li class="singSocial">
                  <a href="'.$socialnetwork[link_social].'">
                    <span class="fa fa-'.$socialnetwork[social].'"></span>
                  </a>
                </li>
              ';
              echo $output;
            }
            echo '</ul>';
          }
        endif;
        echo '</div>';
        ?>


      <a class="brand logo" href="<?= esc_url(home_url('/')); ?>">
        <img class="hidden-md hidden-lg" src="<?php echo get_field('logo_piccolo', 'options') ?>">
        <img class="hidden-xs hidden-sm" src="<?php echo get_field('logo', 'options') ?>">
      </a>
      <img class="tobeactivated hidden-xs hidden-sm" src="<?php echo get_field('logo_piccolo', 'options') ?>">
      <nav class="nav-primary clearfix">
        <?php if (get_field('titolo','options')) {
          echo '<h2 class="tit">'.get_field('titolo','options').'</h2>';
        } ?>
        <div class="menu_btn hidden-md hidden-lg">
          <a>
            <div class="button_container" id="toggle">
              <span class="top"></span>
              <span class="middle"></span>
              <span class="bottom"></span>
            </div>
          </a>
        </div>
        <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav navDesk']);
        endif;
        ?>
        <?php
        $social = get_field('social_network','options');
        if ($social) {
          echo '<ul class="socialNetwork">';
          foreach($social as $socialnetwork) {
            $output = '
              <li class="singSocial">
                <a href="'.$socialnetwork[link_social].'">
                  <span class="fa fa-'.$socialnetwork[social].'"></span>
                </a>
              </li>
            ';
            echo $output;
          }
          echo '</ul>';
        }?>
      </nav>
    </div>
  </div>
</header>
