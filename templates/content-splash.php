<div class="curtain">
</div>
<section class="splash">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-md-6 singSp singSp_sx" style="background:url('<?php echo get_field('img_sx') ?>') center center no-repeat; background-size:cover;">
                <div class="white_bg">
                </div>
                <?php if (get_field('link_sx')) {
                    echo '<a href="'.get_field('link_sx').'">';  
                } else {
                    echo '<a href="#" class="scmBtn">';  
                }?>
                <div class="txt">
                    <?php echo get_field('testo_sx'); ?>
                </div>
                <?php echo '</a>'; ?>
            </div>
            <div class="col-xs-12 col-md-6 singSp singSp_dx" style="background:url('<?php echo get_field('img_dx') ?>') center center no-repeat; background-size:cover;">
                <div class="white_bg">
                </div>
                <?php if (get_field('link_dx')) {
                    echo '<a href="'.get_field('link_dx').'">';  
                } else {
                    echo '<a href="#" class="scmBtn">';  
                }?>
                <div class="txt">
                    <?php echo get_field('testo_dx'); ?>
                </div>
                <?php echo '</a>'; ?>
            </div>
        </div>
    </div>
</section>