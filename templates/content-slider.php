<div class="slider">
          <?php
            $slider = get_field('slider');
            if (is_page_template('template-experience.php')) {
              if (get_field('sottotitolo')) {
                $sott = '<h4>'.get_field('sottotitolo').'</h4>';
              } else {
                $sott = '';
              }
              $outputEXP = '
                <div class="hidden-xs hidden-sm titoli clearfix">
                  <div class="col-xs-12 col-md-7 titolo">
                  <h1>
                  '.get_the_title($post->ID).'
                  </h1>
                  '.$sott.'
                  </div>
                  <div class="col-xs-12 col-md-5 sottotitolo">
                  <p>
                  '.get_field('mini_descrizione').'
                  </p>
                  </div>
                </div>
              ';
              $outputEXPMob = '
              <div class="hidden-lg hidden-md titoli clearfix">
                <div class="col-xs-12 col-md-7 titolo">
                <h1>
                '.get_the_title($post->ID).'
                </h1>
                '.$sott.'
                </div>
                <div class="col-xs-12 col-md-5 sottotitolo">
                <p>
                '.get_field('mini_descrizione').'
                </p>
                </div>
              </div>
            ';
            } else {
              $outputEXP = '';
              $outputEXPMob = '';
            }
          ?>
          <?php if ($slider) {
            if (is_page_template('template-experience.php')) {
              echo '<div class="grad"></div>';
            }
            echo $outputEXP;
            echo '<ul class="slider owl-carousel">';
            foreach($slider as $singole) {
              // controlla X
              if ($singole['posizione'] == 'centro') {
                $cla = 'Xcx';
              } else if ($singole['posizione'] == 'destra') {
                $cla = 'Xdx';
              } else {
                $cla = 'Xsx';
              }
              // controlla Y 
              if ($singole['posizione_copia'] == 'centro') {
                $claY = 'Ycx';
              } else if ($singole['posizione_copia'] == 'alto') {
                $claY = 'Yup';
              } else {
                $claY = 'Ydn';
              }
              // controlla colore H 
              if ($singole['colore_testo_grande'] == 'nero') {
                $H = 'hBlack';
              } else {
                $H = 'hWhite';
              }
              // controlla colore P
              if ($singole['colore_testo_piccolo'] == 'biancosunero') {
                $P = 'BsuN';
              } else {
                $P = 'NsuB';
              }

              $output = '
              <li class="">
              <p class="dida">
              '.$singole['didascalia'].'
              </p>
                <img src="'.$singole['immagine']['sizes']['desktop-slider'].'">
                <div class="clearfix container">
                  <div class="row txtCont">
                    <div class="'.$cla.' '.$H.' '.$P.' '.$claY.' testi '.$singole['posizione'].'">
                    '.$singole['testo'].'
                    </div>
                  </div>
                </div>
              </li>
              ';
              echo $output;
            }
            echo '</ul>';
          } ?>

        </div>
        <div class="mobTit">
          <?php echo $outputEXPMob; ?>
        </div>