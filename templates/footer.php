<footer class="content-info">
  <div class="container">
    <?php the_field('testo_footer', 'options') ?>
  </div>
</footer>

<div class="postFooter">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-4 social clearfix">
        <?php $socials = get_field('social_prefooter','options');
        if ($socials) {
          foreach ($socials as $social) {
            echo '<a href="'.$social[link_singolo_social].'" target="_blank">';
              echo '<p class="fa fa-'.$social[singolo_social].'"></p>';
            echo '</a>';
          }
        }
        ?>
        </div>
      <div class="col-xs-12 col-md-4 logo">
        <a href="http://worldwideshowscorporation.com/" target="_blank">
          <img class="ws" src="<?php the_field('logo_prefooter', 'options') ?>">
        </a>
      </div>
      <div class="col-xs-12 col-md-4 copyright">
        <p>
          <?php the_field('testo_prefooter_dx', 'options') ?>
        </p>
      </div>
    </div>
  </div>
</div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-13056694-13', 'auto');
  ga('send', 'pageview');

</script>
