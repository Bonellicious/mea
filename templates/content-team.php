<section class="team" id="team">
  <div class="container">
    <div class="barra">
    </div>
  </div>
  <?php
      $con = get_field('descrizione_tm');
          if ($con) {
            $cl = 'col-xs-12 col-md-6';
          } else {
            $cl = 'col-xs-12 col-md-12 cent';
          }
        ?>
  <header class="clearfix">
    <div class="container Hcontenitore">
      <div class="row">
        <div class="<?php echo $cl; ?> hstyle">
          <h2>
            <?php echo get_field('titolo_tm'); ?>
          </h2>
        </div>
        <?php
        if (get_field('descrizione_tm')) {
          echo '<div class="col-xs-12 col-md-6 pstyle HCon">';
            echo '<p class="Htxt">'.get_field('descrizione_tm').'</p>';
          echo '</div>';
        }
        ?>
      </div>  
    </div>
  </header>
  <?php
    $sot = get_field('sottotitolo_tm');
    if ($sot) {
      echo '<div class="container">';
      echo '<div class="row sottHeader">';
      echo $sot;
      echo '</div>';
      echo '</div>';
    }
  ?>
  <div class="cont">
  <?php
      $teams = get_field('team');
      if ($teams) {
        echo '<div class="Hcont container">';
        echo '<div class="row">';
        foreach($teams as $team) {
          $url = $team['immagine']['sizes']['team-thumbnail'];
          echo '<div class="col-xs-12 col-md-6 singleTeam sameH">';
            echo '<div class="clearfix">';
                echo '<div class="col-xs-12 col-md-3 imgBgCont">';
                    echo '<img src="'.$url.'"/>';
                echo '</div>';
                echo '<div class="col-xs-12 col-md-9">';
                echo '<h3>'.$team['nome'].'';
                echo '<span>'.$team['ruolo'].'<span></h3>';
                echo $team['testo'];
              echo '</div>';  
            echo '</div>';
          echo '</div>';
        }
        echo '</div>';
        echo '</div>';
      }
    ?>
  </div>
</section>