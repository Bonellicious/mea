/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

    // Use this variable to set up the common and page specific functions. If you
    // rename this variable, you will also need to rename the namespace below.
    var Sage = {
        // All pages
        'common': {
            init: function() {

                function trovaMediaquery() {
                    var ww = $(window).width();
                    if (ww < 768) {
                        $('body').attr('data-mq', 'xs');
                    } else if (ww >= 768 && ww < 992) {
                        $('body').attr('data-mq', 'sm');
                    } else if (ww >= 992 && ww < 1200) {
                        $('body').attr('data-mq', 'md');
                    } else {
                        $('body').attr('data-mq', 'lg');
                    }
                }
          
                $('.splash').imagesLoaded(function() {
                    $('.curtain').fadeOut(200);
                    SplshText();
                });

                $(window).on('scroll', function() {
                    if ($('body').hasClass('ovFl')) {
                    } else {    
                        if ($(window).scrollTop() > 100) {
                            $('header.banner').addClass('active');
                        } else {
                            $('header.banner').removeClass('active');
                        }
                    }
                });

                function SplshText() {
                    if ($('body').hasClass('ovFl')) {

                        $('.scmBtn').on('click', function(){
                            $('body.home').removeClass('ovFl');
                        });

                        $('.splash .txt').each(function(){
                         var hh = $(this).height();
                         $(this).css({
                             top: ($(this).parent().parent().height() - hh + 40) / 2
                         })
                        });        
                    }
                }

                $('.grid').imagesLoaded(function() {
                    // console.log('loaded');
                    $('.grid').masonry({
                        itemSelector: '.grid__item',
                        percentPosition: true,
                    });
                });

                $("#lightgallery").lightGallery();

                $('.menu_btn').on('click', function() {
                    $(this).toggleClass('clicked');
                    $('header.banner').toggleClass('downNow');
                });

                $('.owl-carousel').owlCarousel({
                    items: 1,
                    loop: true,
                    autoplay: true,
                    nav: true,
                    navText: ["<span class='fa fa-chevron-left'>", "<span class='fa fa-chevron-right'>"]
                });

                function gestisciMenu() {
                    $('.nav li a').each(function() {
                        $(this).on('click', function(e) {
                            if ($('header.banner').hasClass('downNow')) {
                                var inPiu = $(window).height() - 140;
                            } else {
                                var inPiu = 0;
                            }
                            var whereD = $(this).attr('title');
                            var target_offset = $("#" + whereD).offset();

                            // controlla se e' home
                            if ($('section').hasClass(whereD)) {
                                e.preventDefault();
                                $('html, body').animate({
                                    scrollTop: target_offset.top - inPiu
                                }, 500);
                                $('header').removeClass('downNow');
                                $('.menu_btn').removeClass('clicked');
                            } else {
                                window.location = window.location.protocol + "//" + window.location.host + "" + "?link=" + whereD;
                            }
                        });
                    });
                }

                // ATTERRA GIUSTO
                function GetURLParameter(sParam) {
                    var sPageURL = window.location.search.substring(1);
                    var sURLVariables = sPageURL.split('&');
                    for (var i = 0; i < sURLVariables.length; i++) {
                        var sParameterName = sURLVariables[i].split('=');
                        if (sParameterName[0] == sParam) {
                            return sParameterName[1];
                        }
                    }
                }​


                var link = GetURLParameter('link');
                if (link) {
                    var inPiu = 0;
                    var target_offset = $("#" + link).offset();

                    $('html, body').animate({
                        scrollTop: target_offset.top - inPiu
                    }, 500);
                }

                function trovailpiugrande(e) {
                    if ($(e).length) {
                        var t = 0; // the height of the highest element (after the function runs)
                        var t_elem; // the highest element (after the function runs)
                        var arrH = [];
                        $(e).each(function() {
                            $this = $(this);
                            var HBlock = $this.outerHeight();
                            arrH.push(HBlock);
                        });
                        var max = arrH.reduce(function(a, b) {
                            return Math.max(a, b);
                        });
                        if ($(window).width() >= 992) {
                            $(e).css({ 'height': max })
                        } else {
                            $(e).css({ 'height': 'auto' })
                        }
                    }
                }


                function posizionaAlCentro() {
                    if ($('.Hcontenitore').length) {
                        $('.Hcontenitore').each(function() {
                            var $HC = $(this).find('.HCon').outerHeight();
                            var $HS = $(this).find('.Htxt').outerHeight();
                            var Giusta = (($HC - $HS) / 2);
                            $(this).find('.Htxt').css({ 'top': Giusta });
                        });
                    }
                }


                if ($('aside.sidebar').length) {
                    $('.contenuto').addClass('yes-sidebar');
                } else {
                    $('.contenuto').addClass('no-sidebar');
                    if ($('.contenuto').find('ul').css('text-align') == 'center') {
                        $('.contenuto').find('ul').addClass('ul-cx');
                    } else if ($('.contenuto').find('ul').css('text-align') == 'left') {
                        $('.contenuto').find('ul').addClass('ul-sx');
                    } else if ($('.contenuto').find('ul').css('text-align') == 'right') {
                        $('.contenuto').find('ul').addClass('ul-dx');
                    }
                }

                posizionaAlCentro();
                trovailpiugrande('.sameH');
                trovaMediaquery();
                gestisciMenu();
                //SplshText();


                $(window).on('resize', function() {
                    trovailpiugrande('.sameH');
                    posizionaAlCentro();
                    trovaMediaquery();
                    SplshText();
                });

            },
            finalize: function() {
                // JavaScript to be fired on all pages, after page specific JS is fired
            }
        },
        // Home page
        'home': {
            init: function() {
                // JavaScript to be fired on the home page
            },
            finalize: function() {
                // JavaScript to be fired on the home page, after the init JS
            }
        },
        // About us page, note the change from about-us to about_us.
        'about_us': {
            init: function() {
                // JavaScript to be fired on the about us page
            }
        }
    };

    // The routing fires all common scripts, followed by the page specific scripts.
    // Add additional events for more control over timing e.g. a finalize event
    var UTIL = {
        fire: function(func, funcname, args) {
            var fire;
            var namespace = Sage;
            funcname = (funcname === undefined) ? 'init' : funcname;
            fire = func !== '';
            fire = fire && namespace[func];
            fire = fire && typeof namespace[func][funcname] === 'function';

            if (fire) {
                namespace[func][funcname](args);
            }
        },
        loadEvents: function() {
            // Fire common init JS
            UTIL.fire('common');

            // Fire page-specific init JS, and then finalize JS
            $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
                UTIL.fire(classnm);
                UTIL.fire(classnm, 'finalize');
            });

            // Fire common finalize JS
            UTIL.fire('common', 'finalize');
        }
    };

    // Load Events
    $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.