<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  
  <?php
  $clasHome = null;
  if (get_field('accendiamo_la_splash') == 'si') {
    $clasHome = ovFl;
  } ?>
  <body <?php body_class($clasHome); ?>>
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php
      do_action('get_header');
      get_template_part('templates/header');
    ?>
    <?php if (is_front_page() && get_field('accendiamo_la_splash')) {
        get_template_part('templates/content', 'splash');
    } ?>
    <div class="wrap" role="document">
      
      <?php get_template_part('templates/content', 'slider'); ?>
      
        <?php if (is_page_template('template-home.php')) { ?>
          <?php echo '<div class="all_cnt">'; ?>
          <?php get_template_part('templates/content', 'activities'); ?>
          <?php get_template_part('templates/content', 'experience'); ?>
          <?php get_template_part('templates/content', 'team'); ?>
          <?php get_template_part('templates/content', 'contacts'); ?>
          <?php echo '</div>'; ?>
        <?php } ?>
      
        <div class="content">
        <?php if (get_field('attivare_sidebar')) {
          $cl = '';
        } else {
          $cl = 'tutto';
        }?>
        <main class="main <?php echo $cl; ?>">
          <?php include Wrapper\template_path(); ?>
        </main><!-- /.main -->
      </div><!-- /.content -->
    </div><!-- /.wrap -->
    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>
  </body>
</html>
